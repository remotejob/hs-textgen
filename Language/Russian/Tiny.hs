{-
  Language.Russian.Tiny v 0.1
  (c) Alexandr A Alexeev 2011 | http://eax.me/
-}
module Language.Russian.Tiny where

-- пол (пока хватает двух)
data Gender = Masculine | Feminine
              deriving (Eq, Show)
-- падежы
data Case = Nominative | Genitive | Dative | Accusative | Ablative | Prepositional
            deriving (Eq, Enum, Bounded, Show)

-- единственное и множественное число (пока не пригождалось)
-- data Number = Singular | Plural

-- окончания существительных в зависимости от пола и падежа
nounEnd :: Gender -> Case -> String
nounEnd g cs =
  endList !! (fromEnum cs)
  where
    endList
      | g == Masculine = ["", "а", "у", "", "ом", "е"] 
      | otherwise = ["а", "ы", "е", "у", "ой", "е"] 

-- окончания прилагательных в зависимости от пола и падежа
defaultAdjectiveEnd :: Gender -> Case -> String
defaultAdjectiveEnd g cs
  | cs == Nominative = if g == Masculine then "ый" else "ая" 
  | cs == Genitive = if g == Masculine then "ого" else "ой"
  | cs == Dative = if g == Masculine then "ому" else "ой" 
  | cs == Accusative = if g == Masculine then "ый" else "ую"
  | cs == Ablative = if g == Masculine then "ым" else "ой"
  | cs == Prepositional = if g == Masculine then "ом" else "ой"
  | otherwise = "(" ++ show cs ++ "," ++ show g ++ ")" 

-- окончания прилагательных, исключения:
-- синИЙ/АЯ, синЕГО/ЕЙ, синЕМУ/ЕЙ, синИЙ/ЮЮ, синИМ/ЕЙ, синЕМ/ЕЙ
specialAdjectiveEnd :: Gender -> Case -> String
specialAdjectiveEnd g cs
  | cs == Nominative = if g == Masculine then "ий" else "яя" 
  | cs == Genitive = if g == Masculine then "его" else "ей"
  | cs == Dative = if g == Masculine then "eму" else "eй" 
  | cs == Accusative = if g == Masculine then "ий" else "юю" -- синИЙ/ЮЮ, но блестящИЙ/УЮ
  | cs == Ablative = if g == Masculine then "им" else "ей"
  | cs == Prepositional = if g == Masculine then "ем" else "ей"
  | otherwise = "(" ++ show cs ++ "," ++ show g ++ ")" 

