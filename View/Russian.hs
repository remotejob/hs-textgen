-- (c) Alexandr A Alexev 2011 | http://eax.me/

module View.Russian (
    showScene,
    showActions
  ) where

import System.Random
import Language.Russian.Tiny
import Model

showScene :: Scene -> IO String
showScene sc = -- TODO: частный случай для одной фигуры и пустой сцены
  showScene' "На сцене находятся: " sc -- TODO: + варианты
  where
    showScene' str [x] = do
      objStr <- objectToString x Nominative
      return $ str ++ objStr ++ "."
    showScene' str (x:xs) = do
      objStr <- objectToString x Nominative
      showScene' (str ++ objStr ++ ", ") xs

showActions :: Scene -> [Action] -> IO (String, Scene)
showActions sc lst =
  showActions' ("", sc) lst
  where
    showActions' rslt [] = do
      return rslt
    showActions' (str, sc) (x:xs) = do
      (newStr, newScene) <- showAction sc x
      showActions' (str ++ newStr ++ " ", newScene) xs

showAction :: Scene -> Action -> IO (String, Scene)
showAction sc act = do
  rslt <- case act of
    Create cpos cobj -> do
      objDescr <- objectToString cobj Accusative
      cposDescr <- showPosition (evalAction sc act) cpos

      ridx <- randomRIO (0, 3) :: IO Int
      let actionWord = ["Создаем", "Кладем", "Помещаем", "Располагаем"] !! ridx

      didx <- randomRIO (0, 4) :: IO Int
      let descEnd = defaultAdjectiveEnd (shapeToGender $ shape cobj) Accusative
      let descrWord = ["", " нов"++descEnd, " тяжел"++descEnd, " увесист"++descEnd, " красив"++descEnd] !! didx

      reverseOrder <- randomRIO (0, 1) :: IO Int
      return $ actionWord ++ (
        if reverseOrder  == 1
          then " " ++ cposDescr ++ descrWord ++ " " ++ objDescr
          else descrWord ++ " " ++ objDescr ++ " " ++ cposDescr ) ++ "."
    Delete dpos -> do
      r <- randomRIO (0, 1) :: IO Int
      rslt <- case r of
        0 -> do
          aidx <- randomRIO (0, 2) :: IO Int
          let actStr = ["Удаляем", "Уничтожаем", "Убираем"] !! aidx
          dposDescr <- showPosition sc dpos
          nidx <- randomRIO (0, 1) :: IO Int
          let objName = ["фигуру", "объект"] !! nidx
          return $ actStr ++ " " ++ objName ++ " " ++ dposDescr
        1 -> do
          aidx <- randomRIO (0, 2) :: IO Int
          let actStr = ["Удаляется", "Уничтожается", "Убирается"] !! aidx
          t <- objectToString (sc !! dpos) Nominative
          return $ actStr ++ " " ++ t
      return $ rslt ++ "."
    Move mfrom mto -> do
      mfromDescr <- showPosition sc mfrom -- TODO : shuffle
      mtoDescr <- showPosition sc mto
      aidx <- randomRIO (0, 1) :: IO Int
      let actStr = ["Меняем местами", "Переставляем"] !! aidx
      return $ actStr ++ " фигуру, находящуюся " ++ mfromDescr ++ " и фигуру, находящуюся " ++ mtoDescr ++ "."
                       -- ^ TODO: объект, расположенный и тп
    _ -> do return "Ничего не делаем :("
  return (rslt, evalAction sc act)

showPosition :: Scene -> Int -> IO String
showPosition sc idx = do
  r <- randomRIO (0, if idx == 0
                     then 1
                     else if length sc > 2 && idx /= length sc - 1
                          then 3 else 2) :: IO Int
  rslt <- case r of
        0 -> return $ if idx == 0 then "на левом краю сцены" else "на позиции " ++ show (idx+1)
        1 -> if idx < (length sc - 1)
          then do
            objDescr <- objectToString ( sc !! (idx+1) ) Ablative
            return $ "перед " ++ objDescr
          else do
            return $ "на правом краю сцены"
        2 -> do
          objDescr <- objectToString ( sc !! (idx-1) ) Genitive
          return $ "после " ++ objDescr
        3 -> do
          afterDescr <- objectToString ( sc !! (idx-1) ) Ablative
          beforeDescr <- objectToString ( sc !! (idx+1) ) Ablative
          reverseOrder <- randomRIO (0, 1) :: IO Int
          return $ "между " ++ (
            if reverseOrder == 1
              then afterDescr ++ " и " ++ beforeDescr
              else beforeDescr ++ " и " ++ afterDescr )
  return rslt 

colorToString :: Color -> Gender -> Case -> String
colorToString c g cs
  | c == Red = "красн" ++ defaultEnd
  | c == Green = "зелен" ++ defaultEnd
  | c == Blue = "син" ++ specialAdjectiveEnd g cs
  | c == Black = "черн" ++ defaultEnd
  | c == White = "бел" ++ defaultEnd
  | c == Orange = "оранжев" ++ defaultEnd
  | c == Yellow = "желт" ++ defaultEnd
  | c == Purple = "фиолетов" ++ defaultEnd
  | c == Brown = "коричнев" ++ defaultEnd
  | otherwise = show c
  where
    defaultEnd = defaultAdjectiveEnd g cs

shapeToGender :: Shape -> Gender
shapeToGender s
  | s == Pyramid || s == Sphere = Feminine
  | otherwise = Masculine

shapeToString :: Shape -> Case -> String
shapeToString s cs
  | s == Cube = "куб" ++ nounEnd g cs
  | s == Pyramid = "пирамид" ++ nounEnd g cs
  | s == Cylinder = "цилиндр" ++ nounEnd g cs
  | s == Parallelepiped = "параллелепипед" ++ nounEnd g cs
  | s == Sphere = "сфер" ++ nounEnd g cs
  | otherwise = show s
  where g = shapeToGender s

objectToString :: Object -> Case -> IO String
objectToString obj cs = do
  r <- randomRIO (0, 5) :: IO Int
  cidx <- randomRIO (0, 3) :: IO Int
  let coloredWord = ["покрашенн", "крашен", "окрашенн", "раскрашенн"] !! cidx
  pidx <- randomRIO (1, 1) :: IO Int
  let (paintWord, paintGender) = [("краской", Feminine), ("цветом", Masculine)] !! pidx
  let rslt = case r of
        0 -> shapeStr ++ " " ++ colorToString (color obj) Masculine Genitive ++ " цвета"
        1 -> colorToString (color obj) shapeGend cs ++ " " ++ shapeStr
        2 -> shapeStr ++ ", " ++ coloredWord ++ defaultAdjectiveEnd shapeGend cs ++
             " в " ++ colorToString (color obj) Masculine Nominative ++ " цвет"
        3 -> coloredWord ++ defaultAdjectiveEnd shapeGend cs ++ 
             " в " ++ colorToString (color obj) Masculine Nominative ++ " цвет " ++ shapeStr
        4 -> shapeStr ++ ", " ++ coloredWord ++ defaultAdjectiveEnd shapeGend cs ++ 
             " " ++ colorToString (color obj) paintGender Ablative ++ " " ++ paintWord
        5 -> coloredWord ++ defaultAdjectiveEnd shapeGend cs ++ " " ++ 
             colorToString (color obj) paintGender Ablative ++ " " ++ paintWord ++
             " " ++ shapeStr
  return rslt
  where
    shapeGend = shapeToGender $ shape obj
    shapeStr = shapeToString (shape obj) cs

