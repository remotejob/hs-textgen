-- (c) Alexandr A Alexev 2011 | http://eax.me/
module Model where

import System.Random
import Data.List

-- цветов очень много: http://whoyougle.ru/services/colour/list
data Color = Red | Green | Blue | Black | White | Orange | Yellow | Purple | Brown
             deriving (Eq, Enum, Bounded, Show)
{-
TODO: розовый, малиновый, салатовый, голубой, бирюзовый, малахитовый, ультрамариновый,
      оливковый, миртовый, черно-красный, кофейный, железно-серый, сине-серый, темно-серый,
      зелено-серый, оливково-серый, коричневато-серый, изумрудный, ливерный, темно-оливковый,
      бордовый, бордово-фиолетовый, мышино-серый, ярко-зеленый, розово-эбонитовый,
      нежно-оливковый, сиреневый, красно-сиреневый, бежево-серый, оливково-коричневый
-}

data Shape = Cube | Pyramid | Cylinder | Parallelepiped | Sphere
             deriving (Eq, Enum, Bounded, Show) -- TODO: конус, октаэдр, призма

data Object = Object { color :: Color, shape :: Shape }
              deriving (Eq, Bounded, Show)

{-
TODO: из чего сделан - лед, стекло, железо, дерево, бетон, нержавейка, свинец, бронза,
      серебро, олово, алмаз, изумруд, камень, кирпич, воск, бумага, картон, пластмасса,
      пластилин, глина...
-}

instance Enum Object where
  fromEnum t = fromEnum (color t) * (1 + fromEnum(maxBound :: Shape)) + fromEnum (shape t)
  toEnum n =
    Object { color = toEnum cn, shape = toEnum sn } 
    where
      cn = n `div` (1 + fromEnum(maxBound :: Shape))
      sn = n `mod` (1 + fromEnum(maxBound :: Shape))

type Scene = [Object]

data Action = Create { pos :: Int, obj :: Object } |
              Delete { pos :: Int } |
              Move { from :: Int, to :: Int } |
              NoAction -- TODO: ChangeShape, ChangeColor, ChangeMaterial, без повторов фигур
              deriving (Eq, Show)

-- создаем случайную сцену
initScene :: Int -> IO Scene
initScene n 
  | n < 1 = initScene 1
  | otherwise = initScene' [] n
  where
  initScene' sc n
    | n <= 0 = do return sc
    | otherwise = do
      action <- generateCreateAction sc
      initScene' (evalAction sc action) (n - 1)

-- случайное Create-действие. используется в initScene и randomAction    
generateCreateAction :: Scene -> IO Action
generateCreateAction s = do
  rpos <- randomRIO (0, length s - 1)
  let possibleNewObjects = [minBound :: Object .. maxBound :: Object] \\ s
  ridx <- randomRIO (0, length possibleNewObjects - 1)
  return Create { pos = rpos, obj = possibleNewObjects !! ridx }

-- случайные действия и сцена после их выполнения
randomActions :: Scene -> Int -> IO ([Action], Scene)
randomActions sc n =
  randomActions' [] sc n
  where
    randomActions' lst sc n
      | n <= 0 = do return (lst, sc)
      | otherwise = do
        (act, sc') <- randomAction sc
        randomActions' (lst ++ [act]) sc' (n - 1)

-- одно случайное действие
randomAction :: Scene -> IO (Action, Scene)
randomAction [] = do return (NoAction, []) 
randomAction s = do
  r <- randomRIO (0, if length s == 1 then 0 else 2) :: IO Int
  rpos <- randomRIO (0, length s - 1)
  action <- case r of
    0 -> if length s == fromEnum (maxBound :: Object) + 1
      then do return Delete { pos = rpos }
      else generateCreateAction s
    1 -> do
      rto <- randomRIO (0, length s - 2)
      return Move { from = rpos, to = ([0..(length s - 1)] \\ [rpos]) !! rto }
    _ -> do
      return Delete { pos = rpos }
  return (action, evalAction s action)

-- меняем состояние сцены в соответсивии с действием
evalAction :: Scene -> Action -> Scene
evalAction sc act =
  case act of
    Create cpos cobj -> let (l, r) = splitAt cpos sc in l ++ [cobj] ++ r
    Delete dpos -> let (l, r) = splitAt dpos sc in l ++ tail r
    Move mfrom mto -> evalMove sc mfrom mto
    _ -> sc
  where
    evalMove sc f t =
      map (
        \(i,v) ->
          if i == f then (sc !! t)
          else if i == t then (sc !! f) else v
      ) $ zip [0..] sc

