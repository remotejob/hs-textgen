import System.Random
import Model
import View.Russian

main = do
  -- создаем сцену
  sceneSize <- randomRIO (1, 3)
  scene <- initScene sceneSize
  sceneBeginView <- showScene scene
  putStr $ sceneBeginView ++ " "

  -- выполняем на ней какие-то действия
  actionsNumber <- randomRIO (2, 4)
  (actions, endScene) <- randomActions scene actionsNumber
  (actionsDescr, _) <- showActions scene actions
  putStr $ actionsDescr

  -- состояние сцены после выполнения всех действий
  sceneEndView <- showScene endScene
  putStrLn sceneEndView

  -- debug
  putStrLn $ show scene
  putStrLn $ show actions
  putStrLn $ show endScene
